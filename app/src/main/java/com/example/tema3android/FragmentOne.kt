package com.example.tema3android

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context.ALARM_SERVICE
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_one.*
import java.util.*


class FragmentOne : Fragment() {

    companion object {
        fun newInstance() = FragmentOne()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_one, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createNotificationChannel()
        btn1.setOnClickListener {
            addFragmentTimePick()
        }
        btn2.setOnClickListener {
            addFragmentDatePick()
        }
        btn3.setOnClickListener {
            Toast.makeText(activity, "Alarm set!", Toast.LENGTH_SHORT).show()
            val intent = Intent(activity, Broadcast::class.java)
            val pendingIntent = PendingIntent.getBroadcast(activity, 0, intent, 0)
            val alarmManager:AlarmManager = activity?.getSystemService(ALARM_SERVICE) as AlarmManager

            var hour = 0;
            var minute = 0;
            val str = time.toString()
            var ok = true
            for(i in str) {
                if(ok)
                {
                    if(i == ':')
                    {
                        ok = false
                    }
                    else
                    {
                        hour = hour*10 + i.toInt()
                    }
                }
                else
                {
                    minute = minute * 10 + i.toInt()
                }
            }

            val minutesToAlarm = (hour*60 + minute) - (Calendar.getInstance().get(Calendar.HOUR)*60 + Calendar.getInstance().get(Calendar.MINUTE))
            val millisecondsToAlarm:Long =  ((minutesToAlarm * 60 - Calendar.getInstance().get(Calendar.SECOND) )* 1000).toLong()
            alarmManager.set(AlarmManager.RTC_WAKEUP, millisecondsToAlarm , pendingIntent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    private fun addFragmentTimePick() {

        val fragment = FragmentTimePick.newInstance()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.main_FrameLayout, fragment, "FragmentTimePick")
        transaction?.hide(this)
        transaction?.addToBackStack("FragmentTimePick")
        transaction?.commit()
    }
    private fun addFragmentDatePick() {

        val fragment = FragmentDatePick.newInstance()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.main_FrameLayout, fragment, "FragmentTimePick")
        transaction?.hide(this)
        transaction?.addToBackStack("FragmentTimePick")
        transaction?.commit()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val channel:NotificationChannel = NotificationChannel("notify", "Channel", NotificationManager.IMPORTANCE_DEFAULT)

        val notificationManager: NotificationManager = activity?.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

}

