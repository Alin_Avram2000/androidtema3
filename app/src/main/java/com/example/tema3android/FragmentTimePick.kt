package com.example.tema3android

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.fragment_one.*
import kotlinx.android.synthetic.main.fragment_time_pick.*


class FragmentTimePick : Fragment() {

    companion object {
        fun newInstance() = FragmentTimePick()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_time_pick, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        timePicker1.setIs24HourView(true);
        setTime.setOnClickListener {
            var hour: Int = timePicker1.hour
            val minute: Int = timePicker1.minute
            activity!!.time.text = hour.toString() + ":" + minute
            backToMainFragment()
        }
    }

    private fun backToMainFragment() {
        val manager: FragmentManager = activity!!.supportFragmentManager
        manager.popBackStack()
    }
}
