package com.example.tema3android

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import kotlinx.android.synthetic.main.fragment_date_pick.*
import kotlinx.android.synthetic.main.fragment_one.*

class FragmentDatePick : Fragment() {

    companion object {
        fun newInstance() = FragmentDatePick()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_date_pick, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setDate.setOnClickListener {
            activity!!.date.text = datePicker1.getDayOfMonth().toString() + "/"+ datePicker1.getMonth() + "/"+datePicker1.getYear()
            backToMainFragment()
        }

    }

    private fun backToMainFragment() {
        val manager: FragmentManager = activity!!.supportFragmentManager
        manager.popBackStack()
    }
}
